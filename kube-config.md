# Kubernetes initial setup

## Namespaces

### Production:

```yaml
---
kind: Namespace
apiVersion: v1
metadata:
  name: production
  labels:
    name: production
```

### Staging:

```yaml
---
kind: Namespace
apiVersion: v1
metadata:
  name: staging
  labels:
    name: staging
```

```
kubectl apply -f ..
```